from django.shortcuts import render
from .models import Worker
# Create your views here.


def main(request):
    top = Worker.objects.filter(chief=None)
    context = {'workers': top}
    return render(request, 'jobapp/index.html', context)


def under(request, id):
    chief = Worker.objects.get(id=id)
    workers = chief.workers.all()
    return render(request, 'jobapp/index.html', {'workers': workers})


def allworkers(request):
    if 'sort_params' not in request.session:
        request.session['sort_params'] = {}
    if request.GET['sort'] in request.session['sort_params']:
        if request.session['sort_params'] == 0:
            allworkers = Worker.objects.order_by(request.GET['sort']).reverse()
            request.session['sort_params'] = {request.get['sort']: 1}
            return render(request, 'jobapp/allworkers.html', {'allworkers': allworkers})
        elif request.session['sort_params'] == 1 or request.GET['sort'] not in request.session['sort_params']:
            request.session['sort_params'] = {request.get['sort']: 0}
            allworkers = Worker.objects.order_by(request.GET['sort'])
            return render(request, 'jobapp/allworkers.html', {'allworkers': allworkers})
    else:
        allworkers = Worker.objects.all()
        return render(request, 'jobapp/allworkers.html', {'allworkers': allworkers})
