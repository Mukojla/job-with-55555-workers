# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-12-19 14:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Worker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Имя')),
                ('surname', models.CharField(max_length=60, verbose_name='Фамилия')),
                ('patronymic', models.CharField(max_length=60, verbose_name='По-батюшки')),
                ('position', models.CharField(max_length=60, verbose_name='Позиция')),
                ('employment_date', models.DateField(verbose_name='Дата устройства')),
                ('salary', models.FloatField(verbose_name='Зарплата')),
                ('thumb', models.ImageField(upload_to='photos/', verbose_name='Фото')),
                ('chief', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='workers', to='jobapp.Worker', verbose_name='Начальник')),
            ],
        ),
    ]
