import json
import random

def get_word():
	alpha = 'abcdefghijklmnopqrstuvwxyz'
	min_len = 5
	max_len = 12
	result = ''
	for i in range(0, random.randint(min_len, max_len)):
		result += random.choice(alpha)
	return result.capitalize()

def get_num():
	return float(random.randint(10000, 100000))

def get_date():
	y = random.randint(2008, 2018)
	m = str(random.randint(1, 12)).rjust(2, '0')
	d = str(random.randint(1, 28)).rjust(2, '0')
	return '{0}-{1}-{2}'.format(y, m, d)

def get_data(pk, position, chief):
	if chief == 0:
		chief = None
	data = {"model": "jobapp.worker", "pk": pk, "fields": {"name": get_word(), "surname": get_word(), "patronymic": get_word(), "position": position, "employment_date": get_date(), "salary": get_num(), "chief": chief, "thumb": "default.png"}}
	return data

def render():
	result = []
	position = ['Owner', 'Director of marketing', 'Director of sales', 'Management of sales', 'Brand manager']
	pk = 1
	for i in range(0, 5):
		result.append(get_data(pk, position[0], 0))
		pk += 1
			
	for i in range(0, 5):
		for j in range(0, 10):
			result.append(get_data(pk, position[1], i+1))
			pk += 1
			
	for i in range(5, 55):
		for j in range(0, 10):
			result.append(get_data(pk, position[2], i+1))
			pk += 1
			
	for i in range(55, 555):
		for j in range(0, 10):
			result.append(get_data(pk, position[3], i+1))
			pk += 1
			
	for i in range(555, 5555):
		for j in range(0, 10):
			result.append(get_data(pk, position[4], i+1))
			pk += 1
			
			
			
			
	return result

json.dump(render(), open(r'dbdump.json', 'w'), indent=4)