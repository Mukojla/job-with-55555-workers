from django import forms
from .models import Worker


class WorkerForm(forms.ModelForm):
    class Meta:
        model = Worker
        fields = ['name', 'surname', 'patronymic', 'position', 'employment_date',
                                                               'salary', 'chief', 'thumb']
